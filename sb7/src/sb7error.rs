use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct SB7Error(String);

impl SB7Error {
    pub fn new(msg: String) -> Self {
        SB7Error(msg)
    }
}

impl fmt::Display for SB7Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl Error for SB7Error {}
