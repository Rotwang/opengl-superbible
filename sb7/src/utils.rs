use crate::sb7error::SB7Error;
use std::env;
use std::path::{Path, PathBuf};

pub fn environ(var: &str) -> Result<String, SB7Error> {
    env::vars()
        .filter_map(|(k, v)| if k == *var { Some(v) } else { None })
        .last()
        .ok_or_else(|| SB7Error::new(format!("Environment variable <{}> not defined.", var)))
}

pub fn find_file(rel_path: PathBuf) -> Result<PathBuf, SB7Error> {
    let base_dir = environ("CARGO_MANIFEST_DIR")?;
    Ok(Path::new(&base_dir).join(rel_path))
}
