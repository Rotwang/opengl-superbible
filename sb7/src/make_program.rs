use crate::sb7error::SB7Error;
use crate::utils;
use gl::types::*;
use std::collections::HashMap;
use std::error;
use std::ffi::CString;
use std::ffi::{CStr, OsString};
use std::fs::File;
use std::io::Read;
use std::os::raw::c_char;
use std::path::Path;
use std::ptr;

type MakeResult = Result<GLuint, Box<dyn error::Error>>;

#[derive(Default)]
pub struct MakeProgram {
    log_size: usize,
    success: GLint,
    info_log: Vec<GLchar>,
    shaders: Vec<GLuint>,
    shader_types: HashMap<OsString, GLenum>,
}

impl MakeProgram {
    pub fn new() -> Self {
        let log_size = 512;
        let shader_types = vec![
            (OsString::from("vert"), gl::VERTEX_SHADER),
            (OsString::from("frag"), gl::FRAGMENT_SHADER),
            (OsString::from("geom"), gl::GEOMETRY_SHADER),
            (OsString::from("tesc"), gl::TESS_CONTROL_SHADER),
            (OsString::from("tese"), gl::TESS_EVALUATION_SHADER),
            (OsString::from("comp"), gl::COMPUTE_SHADER),
        ]
        .into_iter()
        .collect();

        MakeProgram {
            log_size,
            success: gl::FALSE as GLint,
            info_log: Vec::with_capacity(log_size),
            shaders: Vec::new(),
            shader_types,
        }
    }

    pub fn compile(&mut self, file: &str) -> MakeResult {
        println!("COMPILE SHADER: {}", file);

        let abs_path = utils::find_file(Path::new("shaders").join(file))?;
        let shader_type = *match abs_path.extension() {
            Some(t) => self.shader_types.get(t).ok_or_else(|| {
                SB7Error::new(format!(
                    "Unrecognized shader extension: {}",
                    t.to_str().unwrap_or_default()
                ))
            })?,
            None => {
                return Err(Box::new(SB7Error::new(String::from(
                    "Unknown shader type, aborting!",
                ))))
            }
        };

        let mut source = String::new();
        File::open(abs_path.as_os_str())?.read_to_string(&mut source)?;
        self.compile_str(&source, shader_type)
    }

    pub fn compile_str(&mut self, source: &str, typ: GLenum) -> MakeResult {
        self.success = gl::FALSE as GLint;
        self.info_log.clear();
        let c_str_shdr = CString::new(source.as_bytes())?;

        unsafe {
            let shader = gl::CreateShader(typ);
            gl::ShaderSource(shader, 1, &c_str_shdr.as_ptr(), ptr::null());
            gl::CompileShader(shader);

            gl::GetShaderInfoLog(
                shader,
                self.log_size as GLsizei,
                ptr::null_mut(),
                self.info_log.as_mut_ptr() as *mut GLchar,
            );
            gl::GetShaderiv(shader, gl::COMPILE_STATUS, &mut self.success);

            self.shaders.push(shader);
            self.result(shader, &format!("shader {:#X}", typ))
        }
    }

    pub fn link(&mut self) -> MakeResult {
        self.success = gl::FALSE as GLint;
        self.info_log.clear();

        unsafe {
            let program = gl::CreateProgram();
            println!("LINK PROGRAM: {:#X}", program);

            for s in &self.shaders {
                gl::AttachShader(program, *s);
            }

            gl::LinkProgram(program);
            gl::GetProgramInfoLog(
                program,
                self.log_size as GLsizei,
                ptr::null_mut(),
                self.info_log.as_mut_ptr() as *mut GLchar,
            );
            gl::GetProgramiv(program, gl::LINK_STATUS, &mut self.success);

            self.result(program, "program")
        }
    }

    fn result(&self, output: GLuint, description: &str) -> MakeResult {
        let log = unsafe { CStr::from_ptr(self.info_log.as_ptr() as *const c_char) };
        println!("Log for {}: {:?}", description, log);
        if output == 0 || self.success != gl::TRUE as GLint {
            Err(Box::new(SB7Error::new(log.to_string_lossy().to_string())))
        } else {
            Ok(output)
        }
    }
}

impl Drop for MakeProgram {
    fn drop(&mut self) {
        for s in &self.shaders {
            unsafe {
                gl::DeleteShader(*s);
            }
        }
    }
}
