use crate::SBResult;
use glfw::Key;

pub trait Renderer {
    fn startup(&mut self) -> SBResult {
        Ok(())
    }
    fn render(&mut self, _: f64) {}
    fn shutdown(&mut self) -> SBResult {
        Ok(())
    }
    fn on_resize(&mut self, _: i32, _: i32) {}
    fn on_key_pressed(&mut self, _: Key) {}
}
