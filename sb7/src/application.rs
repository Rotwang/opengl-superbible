use crate::{debug_callback::gl_debug_output, Renderer, SBResult};
use gl::types::*;
use glfw::{Action, Context, Key};
use std::ffi::CStr;
use std::os::raw::c_char;
use std::ptr;
use std::sync::mpsc::Receiver;

#[derive(Default)]
pub struct StateObjects {
    pub vertex_array_object: GLuint,
    pub rendering_program: GLuint,
}

struct ApplicationFlags {
    _fullscreen: bool,
    _vsync: bool,
    cursor: bool,
    stereo: bool,
    debug: bool,
    _robust: bool,
}

pub struct Application {
    title: String,
    pub window_width: u32,
    pub window_height: u32,
    major_version: u32,
    minor_version: u32,
    samples: u32,
    flags: ApplicationFlags,
}

impl Application {
    pub fn new(title: &str) -> Self {
        Application {
            title: title.to_owned(),
            window_width: 800,
            window_height: 600,
            major_version: 4,
            minor_version: 5,
            samples: 0,
            flags: ApplicationFlags {
                _fullscreen: false,
                _vsync: false,
                cursor: true,
                stereo: false,
                debug: true,
                _robust: false,
            },
        }
    }

    pub fn run(&self, mut runner: impl Renderer) -> SBResult {
        let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS)?;
        glfw.window_hint(glfw::WindowHint::ContextVersionMajor(self.major_version));
        glfw.window_hint(glfw::WindowHint::ContextVersionMinor(self.minor_version));
        glfw.window_hint(glfw::WindowHint::OpenGlProfile(
            glfw::OpenGlProfileHint::Core,
        ));
        glfw.window_hint(glfw::WindowHint::OpenGlDebugContext(self.flags.debug));
        glfw.window_hint(glfw::WindowHint::Samples(Some(self.samples)));
        glfw.window_hint(glfw::WindowHint::Stereo(self.flags.stereo));

        let (mut window, events) = glfw
            .create_window(
                self.window_width,
                self.window_height,
                &self.title,
                glfw::WindowMode::Windowed,
            )
            .expect("Failed to create GLFW window");

        window.make_current();

        window.set_key_polling(true);
        window.set_size_polling(true);
        window.set_mouse_button_polling(true);
        window.set_cursor_pos_polling(true);
        window.set_scroll_polling(true);
        window.set_framebuffer_size_polling(true);

        if !self.flags.cursor {
            window.set_cursor_mode(glfw::CursorMode::Hidden);
        }

        gl::load_with(|symbol| window.get_proc_address(symbol) as *const _);

        unsafe {
            let vendor = CStr::from_ptr(gl::GetString(gl::VENDOR) as *const c_char);
            let version = CStr::from_ptr(gl::GetString(gl::VERSION) as *const c_char);
            let renderer = CStr::from_ptr(gl::GetString(gl::RENDERER) as *const c_char);
            eprintln!("VENDOR:   {:?}", vendor);
            eprintln!("VERSION:  {:?}", version);
            eprintln!("RENDERER: {:?}", renderer);

            let mut flags: GLint = 0;
            gl::GetIntegerv(gl::CONTEXT_FLAGS, &mut flags);
            if flags & gl::CONTEXT_FLAG_DEBUG_BIT as GLint != 0 {
                println!("Debugging context is ON");
                gl::Enable(gl::DEBUG_OUTPUT);
                gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
                gl::DebugMessageCallback(Some(gl_debug_output), ptr::null());
                gl::DebugMessageControl(
                    gl::DONT_CARE,
                    gl::DONT_CARE,
                    gl::DONT_CARE,
                    0,
                    ptr::null(),
                    gl::TRUE,
                );
            }

            gl::Viewport(0, 0, self.window_width as i32, self.window_height as i32);
        }

        runner.startup()?;

        while !window.should_close() {
            self.process_events(&mut window, &events, &mut runner);
            runner.render(glfw.get_time());
            window.swap_buffers();
            glfw.poll_events();
        }
        runner.shutdown()
    }

    fn process_events(
        &self,
        window: &mut glfw::Window,
        events: &Receiver<(f64, glfw::WindowEvent)>,
        runner: &mut impl Renderer,
    ) {
        for (_, event) in glfw::flush_messages(events) {
            match event {
                glfw::WindowEvent::FramebufferSize(width, height) => unsafe {
                    gl::Viewport(0, 0, width, height);
                    runner.on_resize(width, height);
                },
                glfw::WindowEvent::Key(Key::Escape, _, Action::Press, _) => {
                    window.set_should_close(true);
                }
                glfw::WindowEvent::Key(key, _, Action::Press, _) => {
                    runner.on_key_pressed(key);
                }
                _ => {}
            }
        }
    }
}
