use std::error;

mod sb7error;
mod utils;

mod make_program;
pub use make_program::MakeProgram;

mod renderer;
pub use renderer::Renderer;

mod application;
pub use application::{Application, StateObjects};

mod debug_callback;

mod ktx;
pub use ktx::KTX;

mod object;
pub use object::SBM;

mod text_overlay;
pub use text_overlay::TextOverlay;

mod color;
pub use color::Color;

pub type SBResult = Result<(), Box<dyn error::Error>>;
