use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::mem::size_of;
use std::{error, fmt, str};

#[repr(C)]
#[derive(Default, Clone, Copy)]
pub struct Header {
    magic: [u8; 4],
    pub(crate) size: u32,
    pub(crate) num_chunks: u32,
    flags: u32,
}

impl Header {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let mut buf = [0; size_of::<Header>()];
        infile.read_exact(&mut buf)?;

        Ok(Header {
            magic: buf[0..4].try_into()?,
            size: u32::from_le_bytes(buf[4..8].try_into()?),
            num_chunks: u32::from_le_bytes(buf[8..12].try_into()?),
            flags: u32::from_le_bytes(buf[12..16].try_into()?),
        })
    }
}

impl fmt::Debug for Header {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Header")
            .field("magic", &str::from_utf8(&self.magic).unwrap_or("Err"))
            .field("size", &self.size)
            .field("num_chunks", &self.num_chunks)
            .field("flags", &self.flags)
            .finish()
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:s{}:c{}:f{}",
            &str::from_utf8(&self.magic).unwrap_or("Err"),
            self.size,
            self.num_chunks,
            self.flags,
        )
    }
}
