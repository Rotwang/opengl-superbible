use crate::object::chunk_header::ChunkHeader;
use gl::types::*;
use std::convert::TryInto;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::mem::size_of;
use std::{error, fmt};

#[derive(Default, Debug)]
pub struct IndexDataChunk {
    pub(crate) header: ChunkHeader,
    pub(crate) index_type: u32,
    pub(crate) index_count: u32,
    pub(crate) index_data_offset: u32,
    pub(crate) index_data_size: usize,
    pub(crate) data: Vec<u8>,
}

impl IndexDataChunk {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let chunk_start = infile.seek(SeekFrom::Current(0))?;
        let header = ChunkHeader::extract(infile)?;
        let mut buf = [0; 12];
        infile.read_exact(&mut buf)?;
        let index_type = u32::from_le_bytes(buf[0..4].try_into()?);
        let index_count = u32::from_le_bytes(buf[4..8].try_into()?);
        let index_data_offset = u32::from_le_bytes(buf[8..12].try_into()?);
        let index_data_size = index_count as usize
            * if index_type == gl::UNSIGNED_SHORT {
                size_of::<GLushort>()
            } else {
                size_of::<GLubyte>()
            };

        infile.seek(SeekFrom::Start(index_data_offset as u64))?;
        let mut data: Vec<u8> = vec![0; index_data_size];
        infile.read_exact(&mut data[0..index_data_size])?;

        infile.seek(SeekFrom::Start(chunk_start + header.size as u64))?;

        Ok(IndexDataChunk {
            header,
            index_type,
            index_count,
            index_data_offset,
            index_data_size,
            data,
        })
    }
}

impl fmt::Display for IndexDataChunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}]:t{}:c{}:o{}",
            self.header, self.index_type, self.index_count, self.index_data_offset
        )
    }
}
