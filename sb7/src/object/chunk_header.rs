use crate::object::chunk_type::ChunkType;
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::mem::size_of;
use std::{error, fmt};

#[derive(Default, Debug)]
pub struct ChunkHeader {
    pub(crate) chunk_type: ChunkType,
    pub(crate) size: u32,
}

impl ChunkHeader {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let c = ChunkType::extract(infile)?;
        let mut s = [0; size_of::<u32>()];
        infile.read_exact(&mut s)?;

        Ok(ChunkHeader {
            chunk_type: c,
            size: u32::from_le_bytes(s.try_into()?),
        })
    }
}

impl fmt::Display for ChunkHeader {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:s{}", self.chunk_type, self.size,)
    }
}
