mod chunk_header;
mod chunk_type;
mod data_chunk;
mod header;
mod index_data_chunk;
mod object_list_chunk;
mod vertex_attrib_chunk;
mod vertex_data_chunk;

use crate::{utils, SBResult};
use chunk_type::ChunkType;
use data_chunk::DataChunk;
use header::Header;
use index_data_chunk::IndexDataChunk;
use object_list_chunk::ObjectListChunk;
use object_list_chunk::ObjectListDecl;
use vertex_attrib_chunk::VertexAttribChunk;
use vertex_data_chunk::VertexDataChunk;

use std::fs::File;
use std::io::Seek;
use std::path::Path;

use gl::types::*;
use std::convert::TryInto;
use std::ffi::c_void;
use std::ptr::null;

const VERTEX_ATTRIB_FLAG_NORMALIZED: u32 = 0x00000001;
const MAX_SUB_OBJECTS: u32 = 256;

#[derive(Default)]
pub struct SBM {
    vao: GLuint,
    sub_object: Vec<ObjectListDecl>,
    index_type: GLuint,
    num_sub_objects: usize,
}

impl SBM {
    pub fn load(&mut self, filename: &str) -> SBResult {
        let mut infile = File::open(
            utils::find_file(Path::new("media").join("objects").join(filename))?.as_os_str(),
        )?;
        let header = Header::extract(&mut infile)?;
        infile.seek(std::io::SeekFrom::Start(header.size as u64))?;

        let mut vertex_attrib_chunk: VertexAttribChunk = Default::default();
        let mut vertex_data_chunk: VertexDataChunk = Default::default();
        let mut index_data_chunk: IndexDataChunk = Default::default();
        let mut sub_object_chunk: ObjectListChunk = Default::default();
        let mut data_chunk: DataChunk = Default::default();
        for _ in 0..header.num_chunks {
            match ChunkType::peek(&mut infile)? {
                ChunkType::ATRB => {
                    vertex_attrib_chunk = VertexAttribChunk::extract(&mut infile)?;
                }
                ChunkType::VRTX => {
                    vertex_data_chunk = VertexDataChunk::extract(&mut infile)?;
                }
                ChunkType::INDX => {
                    index_data_chunk = IndexDataChunk::extract(&mut infile)?;
                }
                ChunkType::OLST => {
                    sub_object_chunk = ObjectListChunk::extract(&mut infile)?;
                }
                ChunkType::DATA => {
                    data_chunk = DataChunk::extract(&mut infile)?;
                }
                _ => {}
            }
        }

        unsafe {
            gl::GenVertexArrays(1, &mut self.vao);
            gl::BindVertexArray(self.vao);
        }

        let mut data_buffer: GLuint = 0;
        if let ChunkType::DATA = data_chunk.header.chunk_type {
            unsafe {
                gl::GenBuffers(1, &mut data_buffer);
                gl::BindBuffer(gl::ARRAY_BUFFER, data_buffer);
                gl::BufferData(
                    gl::ARRAY_BUFFER,
                    data_chunk.data_length.try_into()?,
                    data_chunk.data.as_ptr() as *const c_void,
                    gl::STATIC_DRAW,
                );
            }
        } else {
            let mut data_size: u32 = 0;
            let mut size_used: u32 = 0;

            if let ChunkType::VRTX = vertex_data_chunk.header.chunk_type {
                data_size += vertex_data_chunk.data_size;
            }

            if let ChunkType::INDX = index_data_chunk.header.chunk_type {
                data_size += index_data_chunk.index_data_size as u32;
            }

            unsafe {
                gl::GenBuffers(1, &mut data_buffer);
                gl::BindBuffer(gl::ARRAY_BUFFER, data_buffer);
                gl::BufferData(
                    gl::ARRAY_BUFFER,
                    data_size.try_into()?,
                    null(),
                    gl::STATIC_DRAW,
                );
            }

            if let ChunkType::VRTX = vertex_data_chunk.header.chunk_type {
                unsafe {
                    gl::BufferSubData(
                        gl::ARRAY_BUFFER,
                        0,
                        vertex_data_chunk.data_size.try_into()?,
                        vertex_data_chunk.data.as_ptr() as *const c_void,
                    );
                }
                size_used += vertex_data_chunk.data_offset;
            }

            if let ChunkType::INDX = index_data_chunk.header.chunk_type {
                unsafe {
                    gl::BufferSubData(
                        gl::ARRAY_BUFFER,
                        size_used as isize,
                        index_data_chunk.index_data_size.try_into()?,
                        index_data_chunk.data.as_ptr() as *const c_void,
                    );
                }
            }
        }

        for i in 0..vertex_attrib_chunk.attrib_count as usize {
            unsafe {
                gl::VertexAttribPointer(
                    i.try_into()?,
                    vertex_attrib_chunk.attrib_data[i].size as i32,
                    vertex_attrib_chunk.attrib_data[i].typ,
                    if vertex_attrib_chunk.attrib_data[i].flags & VERTEX_ATTRIB_FLAG_NORMALIZED != 0
                    {
                        gl::TRUE
                    } else {
                        gl::FALSE
                    },
                    vertex_attrib_chunk.attrib_data[i].stride as i32,
                    vertex_attrib_chunk.attrib_data[i].data_offset as u64 as *const c_void,
                );
                gl::EnableVertexAttribArray(i.try_into()?);
            }
        }

        let mut _index_offset = 0;
        if let ChunkType::INDX = index_data_chunk.header.chunk_type {
            unsafe { gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, data_buffer) };
            self.index_type = index_data_chunk.index_type;
            _index_offset = index_data_chunk.index_data_offset;
        } else {
            self.index_type = gl::NONE;
        }

        if let ChunkType::OLST = sub_object_chunk.header.chunk_type {
            if sub_object_chunk.count > MAX_SUB_OBJECTS {
                sub_object_chunk.count = MAX_SUB_OBJECTS;
            }

            self.sub_object = sub_object_chunk.sub_object.to_owned();
            self.num_sub_objects = sub_object_chunk.count.try_into()?;
        } else {
            self.sub_object.push(ObjectListDecl {
                first: 0,
                count: if self.index_type != gl::NONE {
                    index_data_chunk.index_count
                } else {
                    vertex_data_chunk.total_vertices
                },
            });
            self.num_sub_objects = 1;
        }

        unsafe {
            gl::BindVertexArray(0);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, 0);
        }

        Ok(())
    }

    pub fn render(&self) {
        self.render_sub_object(0, 1, 0);
    }

    fn render_sub_object(&self, object_index: u32, instance_count: u32, base_instance: u32) {
        unsafe {
            gl::BindVertexArray(self.vao);

            if self.index_type != gl::NONE {
                gl::DrawElementsInstancedBaseInstance(
                    gl::TRIANGLES,
                    self.sub_object[object_index as usize].count as i32,
                    self.index_type,
                    &self.sub_object[object_index as usize].first as *const _ as *const c_void,
                    instance_count as i32,
                    base_instance,
                );
            } else {
                gl::DrawArraysInstancedBaseInstance(
                    gl::TRIANGLES,
                    self.sub_object[object_index as usize].first as i32,
                    self.sub_object[object_index as usize].count as i32,
                    instance_count as i32,
                    base_instance,
                );
            }
        }
    }
}
