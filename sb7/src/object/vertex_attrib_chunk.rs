use crate::object::chunk_header::ChunkHeader;
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::mem::size_of;
use std::{error, fmt, str};

fn from_slice_u8_to_array_u8_64(bytes: &[u8]) -> Result<[u8; 64], &'static str> {
    let mut array = [0; 64];
    if bytes.len() != 64 {
        return Err("Input should have 64 elements!");
    }
    array.copy_from_slice(bytes);
    Ok(array)
}

#[repr(C)]
pub struct VertexAttribDecl {
    name: [u8; 64],
    pub(crate) size: u32,
    pub(crate) typ: u32,
    pub(crate) stride: u32,
    pub(crate) flags: u32,
    pub(crate) data_offset: u32,
}

#[derive(Default, Debug)]
pub struct VertexAttribChunk {
    header: ChunkHeader,
    pub(crate) attrib_count: u32,
    pub(crate) attrib_data: Vec<VertexAttribDecl>,
}

impl VertexAttribChunk {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let h = ChunkHeader::extract(infile)?;
        let mut c = [0; size_of::<u32>()];
        infile.read_exact(&mut c)?;
        let ac = u32::from_le_bytes(c.try_into()?);

        const DECL_SIZE: usize = size_of::<VertexAttribDecl>();
        let mut decl = Vec::with_capacity(ac as usize * DECL_SIZE);
        for _ in 0..ac {
            let mut buf = [0; DECL_SIZE];
            infile.read_exact(&mut buf)?;
            decl.push(VertexAttribDecl {
                name: from_slice_u8_to_array_u8_64(&buf[0..64])?,
                size: u32::from_le_bytes(buf[64..68].try_into()?),
                typ: u32::from_le_bytes(buf[68..72].try_into()?),
                stride: u32::from_le_bytes(buf[72..76].try_into()?),
                flags: u32::from_le_bytes(buf[76..80].try_into()?),
                data_offset: u32::from_le_bytes(buf[80..84].try_into()?),
            });
        }

        Ok(VertexAttribChunk {
            header: h,
            attrib_count: ac,
            attrib_data: decl,
        })
    }
}

impl Default for VertexAttribDecl {
    fn default() -> Self {
        VertexAttribDecl {
            name: [0; 64],
            size: 0,
            typ: 0,
            stride: 0,
            flags: 0,
            data_offset: 0,
        }
    }
}

impl fmt::Debug for VertexAttribDecl {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("VertexAttribDecl")
            .field("chunk_name", &str::from_utf8(&self.name).unwrap_or("Err"))
            .field("size", &self.size)
            .field("type", &self.typ)
            .field("stride", &self.stride)
            .field("flags", &self.flags)
            .field("data_offset", &self.data_offset)
            .finish()
    }
}

impl fmt::Display for VertexAttribChunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}]:c{}", self.header, self.attrib_count,)
    }
}
