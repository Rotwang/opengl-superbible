use std::fs::File;
use std::io::{Read, Seek};
use std::{error, fmt};

pub enum ChunkType {
    INDX,
    VRTX,
    ATRB,
    OLST,
    CMNT,
    DATA,
    UNKN,
}

impl ChunkType {
    fn fourcc(a: char, b: char, c: char, d: char) -> u32 {
        (a as u32) | (b as u32) << 8 | (c as u32) << 16 | (d as u32) << 24
    }

    pub fn peek(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let ret = ChunkType::extract(infile);
        infile.seek(std::io::SeekFrom::Current(-4))?;
        ret
    }

    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let mut m = [0; 4];
        infile.read_exact(&mut m)?;

        let chunk_type = ChunkType::fourcc(m[0] as char, m[1] as char, m[2] as char, m[3] as char);
        Ok(if chunk_type == ChunkType::fourcc('I', 'N', 'D', 'X') {
            ChunkType::INDX
        } else if chunk_type == ChunkType::fourcc('V', 'R', 'T', 'X') {
            ChunkType::VRTX
        } else if chunk_type == ChunkType::fourcc('A', 'T', 'R', 'B') {
            ChunkType::ATRB
        } else if chunk_type == ChunkType::fourcc('O', 'L', 'S', 'T') {
            ChunkType::OLST
        } else if chunk_type == ChunkType::fourcc('C', 'M', 'N', 'T') {
            ChunkType::CMNT
        } else if chunk_type == ChunkType::fourcc('D', 'A', 'T', 'A') {
            ChunkType::DATA
        } else {
            ChunkType::UNKN
        })
    }
}

impl fmt::Display for ChunkType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ChunkType::INDX => "Index",
                ChunkType::VRTX => "Vertex",
                ChunkType::ATRB => "Attribute",
                ChunkType::OLST => "ObjectList",
                ChunkType::CMNT => "Comment",
                ChunkType::DATA => "Data",
                ChunkType::UNKN => "Unknown",
            }
        )
    }
}

impl fmt::Debug for ChunkType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ChunkType")
            .field("variant", &self.to_string())
            .finish()
    }
}

impl Default for ChunkType {
    fn default() -> Self {
        ChunkType::UNKN
    }
}
