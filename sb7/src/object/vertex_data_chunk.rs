use crate::object::chunk_header::ChunkHeader;
use std::convert::TryInto;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::{error, fmt};

#[derive(Default, Debug)]
pub struct VertexDataChunk {
    pub(crate) header: ChunkHeader,
    pub(crate) data_size: u32,
    pub(crate) data_offset: u32,
    pub(crate) total_vertices: u32,
    pub(crate) data: Vec<u8>,
}

impl VertexDataChunk {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let chunk_start = infile.seek(SeekFrom::Current(0))?;
        let header = ChunkHeader::extract(infile)?;
        let mut buf = [0; 12];
        infile.read_exact(&mut buf)?;

        let data_size = u32::from_le_bytes(buf[0..4].try_into()?);
        let data_offset = u32::from_le_bytes(buf[4..8].try_into()?);
        let total_vertices = u32::from_le_bytes(buf[8..12].try_into()?);

        infile.seek(SeekFrom::Start(data_offset as u64))?;
        let mut data: Vec<u8> = vec![0; data_size as usize];
        infile.read_exact(&mut data[0..data_size as usize])?;

        infile.seek(SeekFrom::Start(chunk_start + header.size as u64))?;

        Ok(VertexDataChunk {
            header,
            data_size,
            data_offset,
            total_vertices,
            data,
        })
    }
}

impl fmt::Display for VertexDataChunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}]:s{}:o{}:v{}",
            self.header, self.data_size, self.data_offset, self.total_vertices
        )
    }
}
