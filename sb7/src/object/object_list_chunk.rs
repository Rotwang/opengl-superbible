use crate::object::chunk_header::ChunkHeader;
use std::convert::TryInto;
use std::fs::File;
use std::io::Read;
use std::mem::size_of;
use std::{error, fmt};

#[derive(Default, Debug)]
pub struct ObjectListChunk {
    pub(crate) header: ChunkHeader,
    pub(crate) count: u32,
    pub(crate) sub_object: Vec<ObjectListDecl>,
}

#[repr(C)]
#[derive(Default, Debug, Clone, Copy)]
pub struct ObjectListDecl {
    pub(crate) first: u32,
    pub(crate) count: u32,
}

impl ObjectListChunk {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let header = ChunkHeader::extract(infile)?;
        let mut buf = [0; 4];
        infile.read_exact(&mut buf)?;
        let count = u32::from_le_bytes(buf.try_into()?);

        const DECL_SIZE: usize = size_of::<ObjectListDecl>();
        let mut sub_object = Vec::with_capacity(count as usize * DECL_SIZE);
        for _ in 0..count {
            let mut buf = [0; DECL_SIZE];
            infile.read_exact(&mut buf)?;
            sub_object.push(ObjectListDecl {
                first: u32::from_le_bytes(buf[0..4].try_into()?),
                count: u32::from_le_bytes(buf[4..8].try_into()?),
            });
        }

        Ok(ObjectListChunk {
            header,
            count,
            sub_object,
        })
    }
}

impl fmt::Display for ObjectListChunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "[{}]:c{}", self.header, self.count)
    }
}
