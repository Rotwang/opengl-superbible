use crate::object::chunk_header::ChunkHeader;
use std::convert::TryInto;
use std::fs::File;
use std::io::{Read, Seek, SeekFrom};
use std::{error, fmt};

#[derive(Default, Debug)]
pub struct DataChunk {
    pub(crate) header: ChunkHeader,
    encoding: u32,
    pub(crate) data_offset: u32,
    pub(crate) data_length: u32,
    pub(crate) data: Vec<u8>,
}

impl DataChunk {
    pub fn extract(infile: &mut File) -> Result<Self, Box<dyn error::Error>> {
        let chunk_start_pos = infile.seek(SeekFrom::Current(0))?;
        let header = ChunkHeader::extract(infile)?;

        let mut buf = [0; 12];
        infile.read_exact(&mut buf)?;
        let encoding = u32::from_le_bytes(buf[0..4].try_into()?);
        let data_offset = u32::from_le_bytes(buf[4..8].try_into()?);
        let data_length: u32 = u32::from_le_bytes(buf[8..12].try_into()?);

        let mut data: Vec<u8> = vec![0; data_length as usize];
        infile.seek(SeekFrom::Start(chunk_start_pos + data_offset as u64))?;
        infile.read_exact(&mut data[0..data_length as usize])?;
        infile.seek(SeekFrom::Start(chunk_start_pos + header.size as u64))?;

        Ok(DataChunk {
            header,
            encoding,
            data_offset,
            data_length,
            data,
        })
    }
}

impl fmt::Display for DataChunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "[{}]:e{}:o{}:l{}",
            self.header, self.encoding, self.data_offset, self.data_length
        )
    }
}
