use crate::sb7error::SB7Error;
use crate::utils;
use gl::types::*;
use std::fs::File;
use std::io::{Read, Seek};
use std::mem::size_of;
use std::os::raw::c_void;
use std::path::Path;
use std::{error, slice};

pub struct KTX;

#[repr(C)]
#[derive(Default, Debug, Clone, Copy)]
struct Header {
    identifier: [u8; 12],
    endianness: u32,
    gltype: u32,
    gltypesize: u32,
    glformat: u32,
    glinternalformat: u32,
    glbaseinternalformat: u32,
    pixelwidth: u32,
    pixelheight: u32,
    pixeldepth: u32,
    arrayelements: u32,
    faces: u32,
    miplevels: u32,
    keypairbytes: u32,
}

impl KTX {
    pub fn load(filename: &str, mut tex: GLuint) -> Result<GLuint, Box<dyn error::Error>> {
        let mut h: Header = Default::default();
        let mut f = File::open(
            utils::find_file(Path::new("media").join("textures").join(filename))?.as_os_str(),
        )?;

        unsafe {
            let h_slice =
                slice::from_raw_parts_mut(&mut h as *mut _ as *mut u8, size_of::<Header>());
            f.read_exact(h_slice)?;
        }

        if h.endianness != 0x04030201 {
            return Err(Box::new(SB7Error::new(
                "Not little endian (big endian not implemented)".to_string(),
            )));
        }

        // Guess target (texture type)
        #[allow(clippy::collapsible_if)]
        let target = if h.pixelheight == 0 {
            if h.arrayelements == 0 {
                gl::TEXTURE_1D
            } else {
                gl::TEXTURE_1D_ARRAY
            }
        } else if h.pixeldepth == 0 {
            if h.arrayelements == 0 {
                if h.faces == 0 {
                    gl::TEXTURE_2D
                } else {
                    gl::TEXTURE_CUBE_MAP
                }
            } else {
                if h.faces == 0 {
                    gl::TEXTURE_2D_ARRAY
                } else {
                    gl::TEXTURE_CUBE_MAP_ARRAY
                }
            }
        } else {
            gl::TEXTURE_3D
        };

        unsafe {
            if tex == 0 {
                gl::GenTextures(1, &mut tex);
            }
            gl::BindTexture(target, tex);
        }

        let data_start = f.seek(std::io::SeekFrom::Current(0))? + h.keypairbytes as u64;
        f.seek(std::io::SeekFrom::End(0))?;
        let data_end = f.seek(std::io::SeekFrom::Current(0))? + h.keypairbytes as u64;
        f.seek(std::io::SeekFrom::Start(data_start))?;

        let mut data = vec![0u8; (data_end - data_start) as usize];

        f.read_exact(data.as_mut_slice())?;

        if h.miplevels == 0 {
            h.miplevels = 1;
        }

        match target {
            gl::TEXTURE_1D => unsafe {
                gl::TexStorage1D(
                    gl::TEXTURE_1D,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                );
                gl::TexSubImage1D(
                    gl::TEXTURE_1D,
                    0,
                    0,
                    h.pixelwidth as i32,
                    h.glformat,
                    h.glinternalformat,
                    data.as_ptr() as *const c_void,
                );
            },
            gl::TEXTURE_2D => unsafe {
                if h.gltype == gl::NONE {
                    gl::CompressedTexImage2D(
                        gl::TEXTURE_2D,
                        0,
                        h.glinternalformat,
                        h.pixelwidth as i32,
                        h.pixelheight as i32,
                        0,
                        420 * 380 / 2,
                        data.as_ptr() as *const c_void,
                    );
                } else {
                    gl::TexStorage2D(
                        gl::TEXTURE_2D,
                        h.miplevels as i32,
                        h.glinternalformat,
                        h.pixelwidth as i32,
                        h.pixelheight as i32,
                    );
                    let mut height = h.pixelheight;
                    let mut width = h.pixelwidth;
                    let mut offset: isize = 0;
                    gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
                    for i in 0..h.miplevels as i32 {
                        gl::TexSubImage2D(
                            gl::TEXTURE_2D,
                            i,
                            0,
                            0,
                            width as i32,
                            height as i32,
                            h.glformat,
                            h.gltype,
                            data.as_ptr().offset(offset) as *const c_void,
                        );
                        offset += (height * KTX::calculate_stride(&h, width, 1)) as isize;
                        height >>= 1;
                        width >>= 1;
                        if height < 1 {
                            height = 1;
                        }
                        if width < 1 {
                            width = 1;
                        }
                    }
                }
            },
            gl::TEXTURE_3D => unsafe {
                gl::TexStorage3D(
                    gl::TEXTURE_3D,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    h.pixeldepth as i32,
                );
                gl::TexSubImage3D(
                    gl::TEXTURE_3D,
                    0,
                    0,
                    0,
                    0,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    h.pixeldepth as i32,
                    h.glformat,
                    h.gltype,
                    data.as_ptr() as *const c_void,
                );
            },
            gl::TEXTURE_1D_ARRAY => unsafe {
                gl::TexStorage2D(
                    gl::TEXTURE_1D_ARRAY,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                    h.arrayelements as i32,
                );
                gl::TexSubImage2D(
                    gl::TEXTURE_1D_ARRAY,
                    0,
                    0,
                    0,
                    h.pixelwidth as i32,
                    h.arrayelements as i32,
                    h.glformat,
                    h.gltype,
                    data.as_ptr() as *const c_void,
                );
            },
            gl::TEXTURE_2D_ARRAY => unsafe {
                gl::TexStorage3D(
                    gl::TEXTURE_2D_ARRAY,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    h.arrayelements as i32,
                );
                gl::TexSubImage3D(
                    gl::TEXTURE_2D_ARRAY,
                    0,
                    0,
                    0,
                    0,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    h.arrayelements as i32,
                    h.glformat,
                    h.gltype,
                    data.as_ptr() as *const c_void,
                );
            },
            gl::TEXTURE_CUBE_MAP => unsafe {
                gl::TexStorage2D(
                    gl::TEXTURE_CUBE_MAP,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                );

                let face_size: u32 = KTX::calculate_face_size(&h);
                for i in 0..h.faces {
                    gl::TexSubImage2D(
                        gl::TEXTURE_CUBE_MAP_POSITIVE_X + i,
                        0,
                        0,
                        0,
                        h.pixelwidth as i32,
                        h.pixelheight as i32,
                        h.glformat,
                        h.gltype,
                        data.as_ptr().offset((face_size * i) as isize) as *const c_void,
                    );
                }
            },
            gl::TEXTURE_CUBE_MAP_ARRAY => unsafe {
                gl::TexStorage3D(
                    gl::TEXTURE_CUBE_MAP_ARRAY,
                    h.miplevels as i32,
                    h.glinternalformat,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    h.arrayelements as i32,
                );
                gl::TexSubImage3D(
                    gl::TEXTURE_CUBE_MAP_ARRAY,
                    0,
                    0,
                    0,
                    0,
                    h.pixelwidth as i32,
                    h.pixelheight as i32,
                    (h.faces * h.arrayelements) as i32,
                    h.glformat,
                    h.gltype,
                    data.as_ptr() as *const c_void,
                );
            },
            _ => {
                return Err(Box::new(SB7Error::new(format!(
                    "Unknown texture type: {}",
                    target
                ))));
            }
        }

        if h.miplevels == 1 {
            unsafe {
                gl::GenerateMipmap(target);
            }
        }

        Ok(tex)
    }

    fn calculate_stride(h: &Header, width: u32, pad: u32) -> u32 {
        let channels = match h.glbaseinternalformat {
            gl::RED => 1u32,
            gl::RG => 2u32,
            gl::BGR => 3u32,
            gl::BGRA | gl::RGBA => 4u32,
            _ => 0,
        };

        let stride = h.gltypesize * channels * width;
        (stride + (pad - 1)) & !(pad - 1)
    }

    fn calculate_face_size(h: &Header) -> u32 {
        let stride: u32 = KTX::calculate_stride(&h, h.pixelwidth, 4);

        stride * h.pixelheight
    }
}
