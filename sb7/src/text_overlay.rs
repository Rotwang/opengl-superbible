use crate::{MakeProgram, KTX};
use gl::types::*;
use std::convert::TryInto;
use std::error::Error;
use std::ffi::c_void;

#[derive(Default, Debug)]
pub struct TextOverlay {
    text_buffer: GLuint,
    font_texture: GLuint,
    vao: GLuint,
    text_program: GLuint,
    screen_buffer: Vec<u8>,
    dirty: bool,
    cursor_x: i32,
    cursor_y: i32,
    buffer_width: i32,
    buffer_height: i32,
}

const VERTEX_SHADER_SOURCE: &str = r#"
#version 440 core
void main(void) {
    gl_Position = vec4(float((gl_VertexID >> 1) & 1) * 2.0 - 1.0,
                       float((gl_VertexID & 1)) * 2.0 - 1.0,
                       0.0, 1.0);
}
"#;

const FRAGMENT_SHADER_SOURCE: &str = r#"
#version 440 core
layout (origin_upper_left) in vec4 gl_FragCoord;
layout (location = 0) out vec4 o_color;
layout (binding = 0) uniform isampler2D text_buffer;
layout (binding = 1) uniform isampler2DArray font_texture;
void main(void) {
    ivec2 frag_coord = ivec2(gl_FragCoord.xy);
    ivec2 char_size = textureSize(font_texture, 0).xy;
    ivec2 char_location = frag_coord / char_size;
    ivec2 texel_coord = frag_coord % char_size;
    int character = texelFetch(text_buffer, char_location, 0).x;
    float val = texelFetch(font_texture, ivec3(texel_coord, character), 0).x;
    if (val == 0.0)
        discard;
    o_color = vec4(1.0);
}
"#;

impl TextOverlay {
    pub fn new(width: i32, height: i32, font: &str) -> Result<Self, Box<dyn Error>> {
        let mut make = MakeProgram::new();
        make.compile_str(VERTEX_SHADER_SOURCE, gl::VERTEX_SHADER)?;
        make.compile_str(FRAGMENT_SHADER_SOURCE, gl::FRAGMENT_SHADER)?;
        let text_program = make.link()?;
        let mut vao = 0;
        let mut text_buffer = 0;

        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);

            gl::GenTextures(1, &mut text_buffer);
            gl::BindTexture(gl::TEXTURE_2D, text_buffer);
            gl::TexStorage2D(gl::TEXTURE_2D, 1, gl::R8UI, width, height);
        }
        let font_texture = KTX::load(
            if font.is_empty() {
                "cp437_9x16.ktx"
            } else {
                font
            },
            0,
        )?;

        let screen_buffer = vec![0; (width * height).try_into()?];

        Ok(TextOverlay {
            text_buffer,
            font_texture,
            vao,
            text_program,
            screen_buffer,
            buffer_height: height,
            buffer_width: width,
            ..Default::default()
        })
    }

    pub fn clear(&mut self) {
        //self.screen_buffer.iter_mut().map(|x| *x = 0);
        //self.screen_buffer.clear();
        let zeros = vec![0; self.screen_buffer.len()];
        self.screen_buffer.clone_from(&zeros);
        self.dirty = true;
        self.cursor_x = 0;
        self.cursor_y = 0;
    }

    pub fn draw_text(&mut self, text: &str, x: i32, y: i32) {
        let cursor = (y * self.buffer_width + x) as usize;
        self.screen_buffer[cursor..text.as_bytes().len()].clone_from_slice(text.as_bytes());
        self.dirty = true;
    }

    pub fn draw(&mut self) {
        unsafe {
            gl::UseProgram(self.text_program);
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.text_buffer);
            if self.dirty {
                gl::TexSubImage2D(
                    gl::TEXTURE_2D,
                    0,
                    0,
                    0,
                    self.buffer_width,
                    self.buffer_height,
                    gl::RED_INTEGER,
                    gl::UNSIGNED_BYTE,
                    self.screen_buffer.as_ptr() as *const c_void,
                );
                self.dirty = false;
            }
            gl::ActiveTexture(gl::TEXTURE1);
            gl::BindTexture(gl::TEXTURE_2D_ARRAY, self.font_texture);

            gl::BindVertexArray(self.vao);
            gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
        }
    }
}
