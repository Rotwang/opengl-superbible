use vmath::vec4;

pub struct Color;

impl Color {
    pub fn black() -> vec4 {
        colorfromhex(0x000000)
    }
}

fn colorfromhex(hex: u32) -> vec4 {
    vec4::new(
        ((hex >> 16) & 0xFF) as f32 / 255f32,
        ((hex >> 8) & 0xFF) as f32 / 255f32,
        ((hex >> 0) & 0xFF) as f32 / 255f32,
        1f32,
    )
}
