use std::ops::Mul;

#[allow(non_camel_case_types)]
pub type mat4 = Tmat4<f32>;
#[allow(non_camel_case_types)]
pub type vec4 = Tvec4<f32>;

#[derive(Debug, Copy, Clone)]
pub struct Tvec4<T> {
    data: [T; 4],
}

impl<T> Tvec4<T> {
    pub fn new(x: T, y: T, z: T, w: T) -> Self {
        Tvec4 { data: [x, y, z, w] }
    }

    pub fn as_ptr(&self) -> *const T {
        self.data.as_ptr() as *const T
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Tmat4<T> {
    data: [[T; 4]; 4],
}

impl<T> Tmat4<T> {
    pub fn as_ptr(&self) -> *const T {
        self.data.as_ptr() as *const T
    }
}

impl Mul for mat4 {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self {
        let mut result = mat4::default();
        for j in 0..4 {
            for i in 0..4 {
                let mut sum = 0 as f32;
                for n in 0..4 {
                    sum += self.data[n][i] * rhs.data[j][n];
                }
                result.data[j][i] = sum;
            }
        }
        result
    }
}

#[rustfmt::skip]
impl Default for mat4 {
    fn default() -> Self {
        mat4 {
            data: [
                [1f32, 0f32, 0f32, 0f32],
                [0f32, 1f32, 0f32, 0f32],
                [0f32, 0f32, 1f32, 0f32],
                [0f32, 0f32, 0f32, 1f32],
            ],
        }
    }
}

#[allow(non_snake_case)]
#[rustfmt::skip]
pub fn perspective(fovy: f32, aspect: f32, n: f32, f: f32) -> mat4 {
    let q: f32 = 1.0f32 / (0.5f32 * fovy).to_radians().tan();
    let A: f32 = q / aspect;
    let B: f32 = (n + f) / (n - f);
    let C: f32 = (2.0f32 * n * f) / (n - f);

    mat4 {
        data: [
            [A,    0f32, 0f32, 0f32],
            [0f32,    q, 0f32, 0f32],
            [0f32, 0f32, B,   -1f32],
            [0f32, 0f32, C,    0f32],
        ],
    }
}

#[rustfmt::skip]
pub fn translate(x: f32, y: f32, z: f32) -> mat4 {
    mat4 {
        data: [
            [1f32, 0f32, 0f32, 0f32],
            [0f32, 1f32, 0f32, 0f32],
            [0f32, 0f32, 1f32, 0f32],
            [x,    y,    z,    1f32],
        ]
    }
}

#[allow(clippy::excessive_precision)]
#[allow(clippy::many_single_char_names)]
#[rustfmt::skip]
pub fn rotate(angle: f32, x: f32, y: f32, z: f32) -> mat4 {
    let x2 = x * x;
    let y2 = y * y;
    let z2 = z * z;
    let rads = angle * 0.017_453_292_5_f32;
    let c = rads.cos();
    let s = rads.sin();
    let omc = 1f32 - c;

    mat4 {
        data: [
            [x2 * omc + c,        y * x * omc + z * s, x * z * omc - y * s, 0f32],
            [x * y * omc - z * s, y2 * omc + c,        y * z * omc + x * s, 0f32],
            [x * z * omc + y * s, y * z * omc - x * s, z2 * omc + c,        0f32],
            [0f32,                0f32,                0f32,                1f32],
        ]
    }
}

#[rustfmt::skip]
pub fn scale_non_uniform(x: f32, y: f32, z: f32) -> mat4 {
    mat4 {
        data: [
            [x, 0f32, 0f32, 0f32],
            [0f32, y, 0f32, 0f32],
            [0f32, 0f32, z, 0f32],
            [0f32, 0f32, 0f32, 1f32],
        ]
    }
}

pub fn scale(x: f32) -> mat4 {
    scale_non_uniform(x, x, x)
}
