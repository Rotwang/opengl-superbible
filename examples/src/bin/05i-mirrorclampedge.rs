use gl::types::*;
use sb7::{Application, Color, MakeProgram, Renderer, SBResult, StateObjects, TextOverlay, KTX};
use std::{thread, time};
use glfw::Key;

#[derive(Copy, Clone, Debug)]
enum DisplayMode {
    ClampToBorder,
    MirrorClampToEdge,
    MaxMode,
}

impl DisplayMode {
    fn from_i32(v: i32) -> Self {
        match v {
            _ if v == DisplayMode::ClampToBorder as i32 => DisplayMode::ClampToBorder,
            _ if v == DisplayMode::MirrorClampToEdge as i32 => DisplayMode::MirrorClampToEdge,
            _ => DisplayMode::ClampToBorder,
        }
    }
}

impl Default for DisplayMode {
    fn default() -> Self {
        DisplayMode::ClampToBorder
    }
}

#[derive(Default)]
struct MirrorClampEdge {
    s: StateObjects,
    overlay: TextOverlay,
    input_texture: GLuint,
    dummy_vao: GLuint,
    display_mode: DisplayMode,
}

impl Renderer for MirrorClampEdge {
    fn startup(&mut self) -> SBResult {
        self.overlay = TextOverlay::new(80, 50, "")?;
        self.input_texture = KTX::load("rightarrows.ktx", 0)?;

        let mut make = MakeProgram::new();
        make.compile("05/mirrorclampedge.vert")?;
        make.compile("05/mirrorclampedge.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            gl::GenVertexArrays(1, &mut self.dummy_vao);
        }
        Ok(())
    }

    fn render(&mut self, _current_time: f64) {
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, Color::black().as_ptr());
        }

        thread::sleep(time::Duration::from_secs(1));

        unsafe {
            gl::ActiveTexture(gl::TEXTURE0);
            gl::BindTexture(gl::TEXTURE_2D, self.input_texture);
            gl::BindVertexArray(self.dummy_vao);
            gl::UseProgram(self.s.rendering_program);
        }

        match self.display_mode {
            DisplayMode::ClampToBorder => unsafe {
                gl::TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_WRAP_S,
                    gl::CLAMP_TO_BORDER as i32,
                );
                gl::TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_WRAP_T,
                    gl::CLAMP_TO_BORDER as i32,
                );
            },
            DisplayMode::MirrorClampToEdge => unsafe {
                gl::TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_WRAP_S,
                    gl::MIRROR_CLAMP_TO_EDGE as i32,
                );
                gl::TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_WRAP_T,
                    gl::MIRROR_CLAMP_TO_EDGE as i32,
                );
            },
            _ => {
                eprintln!("Unknown display mode!");
            }
        }

        unsafe {
            gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
        }

        self.overlay.clear();
        self.overlay.draw_text(
            if let DisplayMode::ClampToBorder = self.display_mode {
                "Mode = GL_CLAMP_TO_BORDER (M toggles)"
            } else {
                "Mode = GL_MIRROR_CLAMP_TO_EDGE (M toggles)"
            },
            0,
            0,
        );
        self.overlay.draw();
    }

    fn on_key_pressed(&mut self, key: Key) {
        match key {
            Key::R | Key::Space => {
                println!("{:?}", self.display_mode);
                self.display_mode = DisplayMode::from_i32(self.display_mode as i32 + 1);
                println!("{:?}", self.display_mode);
            }
            _ => {}
        }
    }
}

fn main() -> SBResult {
    let app = Application::new("GL_MIRROR_CLAMP_TO_EDGE");
    app.run(MirrorClampEdge {
        ..Default::default()
    })
}
