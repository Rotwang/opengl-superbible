use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects, KTX};
use std::ffi::CStr;

#[derive(Default)]
struct Uniforms {
    mvp: GLint,
    offset: GLint,
}

#[derive(Default)]
struct Tunnel {
    s: StateObjects,
    window_width: i32,
    window_height: i32,
    uniforms: Uniforms,
    tex_wall: GLuint,
    tex_ceiling: GLuint,
    tex_floor: GLuint,
}

impl Renderer for Tunnel {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile("05/tunnel.vert")?;
        make.compile("05/tunnel.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            self.uniforms.mvp = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"mvp\0").as_ptr(),
            );
            self.uniforms.offset = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"offset\0").as_ptr(),
            );

            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        self.tex_wall = KTX::load("brick.ktx", 0)?;
        self.tex_ceiling = KTX::load("ceiling.ktx", 0)?;
        self.tex_floor = KTX::load("floor.ktx", 0)?;

        let textures = [self.tex_floor, self.tex_wall, self.tex_ceiling];

        for t in &textures {
            unsafe {
                gl::BindTexture(gl::TEXTURE_2D, *t);
                gl::TexParameteri(
                    gl::TEXTURE_2D,
                    gl::TEXTURE_MIN_FILTER,
                    gl::LINEAR_MIPMAP_LINEAR as i32,
                );
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
            }
        }

        unsafe {
            gl::BindVertexArray(self.s.vertex_array_object);
        }
        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let black: [GLfloat; 4] = [0f32, 0f32, 0f32, 0f32];
        let t = current_time as f32;

        unsafe {
            gl::Viewport(0, 0, self.window_width, self.window_height);
            gl::ClearBufferfv(gl::COLOR, 0, black.as_ptr());

            gl::UseProgram(self.s.rendering_program);
        }

        let proj_matrix = vmath::perspective(
            60f32,
            self.window_width as f32 / self.window_height as f32,
            0.1f32,
            1000f32,
        );

        unsafe {
            gl::Uniform1f(self.uniforms.offset, t * 0.003f32);
        }

        let textures = [
            self.tex_wall,
            self.tex_floor,
            self.tex_wall,
            self.tex_ceiling,
        ];
        for i in 0..textures.len() {
            let mv_matrix = vmath::rotate(90f32 * i as f32, 0f32, 0f32, 1f32)
                * vmath::translate(-0.5f32, 0f32, -10f32)
                * vmath::rotate(90f32, 0f32, 1f32, 0f32)
                * vmath::scale_non_uniform(30f32, 1f32, 1f32);
            let mvp = proj_matrix * mv_matrix;
            unsafe {
                gl::UniformMatrix4fv(self.uniforms.mvp, 1, gl::FALSE, mvp.as_ptr());
                gl::BindTexture(gl::TEXTURE_2D, textures[i]);
                gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
            }
        }
    }

    fn on_resize(&mut self, w: i32, h: i32) {
        self.window_width = w;
        self.window_height = h;
    }
}

fn main() -> SBResult {
    let app = Application::new("Tunnel");
    app.run(Tunnel {
        window_width: app.window_width as i32,
        window_height: app.window_height as i32,
        ..Default::default()
    })
}
