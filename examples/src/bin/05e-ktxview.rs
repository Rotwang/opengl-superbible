use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects, KTX};
use std::ffi::CStr;

#[derive(Default)]
struct KTXViewer {
    s: StateObjects,
    texture: GLuint,
    exposure_location: GLint,
}

impl Renderer for KTXViewer {
    fn startup(&mut self) -> SBResult {
        unsafe {
            gl::GenTextures(1, &mut self.texture);
        }

        KTX::load("tree.ktx", self.texture)?;

        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
        }

        let mut make = MakeProgram::new();
        make.compile("05/ktxview.vert")?;
        make.compile("05/ktxview.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            self.exposure_location = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"exposure\0").as_ptr(),
            );

            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let green: [GLfloat; 4] = [0.0f32, 0.25f32, 0.0f32, 1.0f32];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());
            gl::UseProgram(self.s.rendering_program);
            //gl::Viewport(0, 0, self.window_width, self.window_height);
            gl::Uniform1f(
                self.exposure_location,
                current_time.sin() as f32 * 16f32 + 16f32,
            );
            gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteVertexArrays(1, &self.s.vertex_array_object);
            gl::DeleteTextures(1, &self.texture);
        }
        Ok(())
    }
}

fn main() -> SBResult {
    let app = Application::new("KTX Viewer");
    app.run(KTXViewer {
        ..Default::default()
    })
}
