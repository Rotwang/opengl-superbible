use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};
use std::ffi::CStr;
use std::mem::size_of_val;
use std::os::raw::c_void;
use std::ptr::null;
use vmath::mat4;

#[derive(Default)]
struct SpinnyCube {
    s: StateObjects,
    window_width: i32,
    window_height: i32,
    buffer: GLuint,
    mv_location: GLint,
    mv_matrix: mat4,
    proj_location: GLint,
    proj_matrix: mat4,
}

impl Renderer for SpinnyCube {
    fn startup(&mut self) -> SBResult {
        println!("{:?}", self.proj_matrix);
        let mut make = MakeProgram::new();
        make.compile("00/sandbox.vert")?;
        make.compile("00/sandbox.frag")?;
        self.s.rendering_program = make.link()?;

        self.mv_location = unsafe {
            gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"mv_matrix\0").as_ptr(),
            )
        };
        self.proj_location = unsafe {
            gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"proj_matrix\0").as_ptr(),
            )
        };

        unsafe {
            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        #[rustfmt::skip]
        let vertex_positions: [GLfloat; 108] = [
            -0.25f32,  0.25f32, -0.25f32,
            -0.25f32, -0.25f32, -0.25f32,
             0.25f32, -0.25f32, -0.25f32,

             0.25f32, -0.25f32, -0.25f32,
             0.25f32,  0.25f32, -0.25f32,
            -0.25f32,  0.25f32, -0.25f32,

             0.25f32, -0.25f32, -0.25f32,
             0.25f32, -0.25f32,  0.25f32,
             0.25f32,  0.25f32, -0.25f32,

             0.25f32, -0.25f32,  0.25f32,
             0.25f32,  0.25f32,  0.25f32,
             0.25f32,  0.25f32, -0.25f32,

             0.25f32, -0.25f32,  0.25f32,
            -0.25f32, -0.25f32,  0.25f32,
             0.25f32,  0.25f32,  0.25f32,

            -0.25f32, -0.25f32,  0.25f32,
            -0.25f32,  0.25f32,  0.25f32,
             0.25f32,  0.25f32,  0.25f32,

            -0.25f32, -0.25f32,  0.25f32,
            -0.25f32, -0.25f32, -0.25f32,
            -0.25f32,  0.25f32,  0.25f32,

            -0.25f32, -0.25f32, -0.25f32,
            -0.25f32,  0.25f32, -0.25f32,
            -0.25f32,  0.25f32,  0.25f32,

            -0.25f32, -0.25f32,  0.25f32,
             0.25f32, -0.25f32,  0.25f32,
             0.25f32, -0.25f32, -0.25f32,

             0.25f32, -0.25f32, -0.25f32,
            -0.25f32, -0.25f32, -0.25f32,
            -0.25f32, -0.25f32,  0.25f32,

            -0.25f32,  0.25f32, -0.25f32,
             0.25f32,  0.25f32, -0.25f32,
             0.25f32,  0.25f32,  0.25f32,

             0.25f32,  0.25f32,  0.25f32,
            -0.25f32,  0.25f32,  0.25f32,
            -0.25f32,  0.25f32, -0.25f32
        ];

        unsafe {
            gl::GenBuffers(1, &mut self.buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                size_of_val(&vertex_positions) as isize,
                vertex_positions.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );
            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 0, null());
            gl::EnableVertexAttribArray(0);

            gl::Enable(gl::CULL_FACE);
            gl::FrontFace(gl::CW);

            gl::Enable(gl::DEPTH_TEST);
            gl::DepthFunc(gl::LEQUAL);
        }

        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let green: [GLfloat; 4] = [0.0f32, 0.25f32, 0.0f32, 1.0f32];
        let one: GLfloat = 1.0f32;

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());
            gl::ClearBufferfv(gl::DEPTH, 0, &one);

            gl::UseProgram(self.s.rendering_program);

            gl::UniformMatrix4fv(self.proj_location, 1, gl::FALSE, self.proj_matrix.as_ptr());

            let _f = current_time as f32 * 0.3f32;
            self.mv_matrix = vmath::translate(0.0f32, 0.0f32, -2.0f32)
                * vmath::rotate(current_time as f32 * 45.0f32, 0.0f32, 1.0f32, 0f32);
            gl::UniformMatrix4fv(self.mv_location, 1, gl::FALSE, self.mv_matrix.as_ptr());
            gl::DrawArrays(gl::TRIANGLES, 0, 36);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.s.vertex_array_object);
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteBuffers(1, &self.buffer);
        }
        Ok(())
    }

    fn on_resize(&mut self, w: i32, h: i32) {
        self.window_width = w;
        self.window_height = h;
        self.proj_matrix = calc_perspective(w, h);
    }
}

fn calc_perspective(w: i32, h: i32) -> mat4 {
    let aspect = w as f32 / h as f32;
    vmath::perspective(50.0f32, aspect, 0.1f32, 1000.0f32)
}

fn main() -> SBResult {
    let app = Application::new("Spinny Cube");
    let w: i32 = app.window_width as i32;
    let h: i32 = app.window_height as i32;
    app.run(SpinnyCube {
        window_width: w,
        window_height: h,
        proj_matrix: calc_perspective(w, h),
        ..Default::default()
    })
}
