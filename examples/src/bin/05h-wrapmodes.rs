use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects, KTX};
use std::ffi::CStr;

#[derive(Default)]
struct WrapModes {
    s: StateObjects,
    offset_location: GLint,
    texture: GLuint,
}

impl Renderer for WrapModes {
    fn startup(&mut self) -> SBResult {
        unsafe {
            gl::GenTextures(1, &mut self.texture);
        }

        KTX::load("rightarrows.ktx", self.texture)?;

        unsafe {
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
        }

        let mut make = MakeProgram::new();
        make.compile("05/wrapmodes.vert")?;
        make.compile("05/wrapmodes.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            self.offset_location = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"offset\0").as_ptr(),
            );

            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        Ok(())
    }

    fn render(&mut self, _current_time: f64) {
        let green: [GLfloat; 4] = [0f32, 0.1f32, 0f32, 1f32];
        let yellow: [GLfloat; 4] = [0.4f32, 0.4f32, 0f32, 1f32];
        let wrapmodes = [
            gl::CLAMP_TO_EDGE,
            gl::REPEAT,
            gl::CLAMP_TO_BORDER,
            gl::MIRRORED_REPEAT,
        ];
        #[rustfmt::skip]
        let offsets: [f32; 8] = [
            -0.5, -0.5,
             0.5, -0.5,
            -0.5,  0.5,
             0.5,  0.5,
        ];

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());
            gl::UseProgram(self.s.rendering_program);
            gl::TexParameterfv(gl::TEXTURE_2D, gl::TEXTURE_BORDER_COLOR, yellow.as_ptr());

            for i in 0..4 {
                gl::Uniform2fv(self.offset_location, 1, &offsets[i * 2]);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, wrapmodes[i] as i32);
                gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, wrapmodes[i] as i32);

                gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
            }
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteVertexArrays(1, &self.s.vertex_array_object);
            gl::DeleteTextures(1, &self.texture);
        }
        Ok(())
    }
}

fn main() -> SBResult {
    let app = Application::new("Texture Wrap Modes");
    app.run(WrapModes {
        ..Default::default()
    })
}
