use gl::types::*;
use sb7::{Application, Renderer, SBResult};

struct SimpleClear;

impl Renderer for SimpleClear {
    fn render(&mut self, _current_time: f64) {
        let red: [GLfloat; 4] = [1.0, 0.0, 0.0, 1.0];

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, red.as_ptr());
        }
    }
}

fn main() -> SBResult {
    Application::new("Simple Clear").run(SimpleClear)
}
