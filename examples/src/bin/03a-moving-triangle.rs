use gl::types::*;

use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};

struct MovingTriangle(StateObjects);

impl Renderer for MovingTriangle {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile("03/moving-triangle.vert")?;
        make.compile("03/moving-triangle.frag")?;
        self.0.rendering_program = make.link()?;

        unsafe {
            gl::CreateVertexArrays(1, &mut self.0.vertex_array_object);
            gl::BindVertexArray(self.0.vertex_array_object);
        }
        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let color: [GLfloat; 4] = [0.0f32, 0.2f32, 0.0f32, 1.0f32];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, color.as_ptr());
            let attributes: [GLfloat; 4] = [
                (current_time.sin() * 0.5f64) as GLfloat,
                (current_time.cos() * 0.5f64) as GLfloat,
                0.0f32,
                0.0f32,
            ];
            gl::UseProgram(self.0.rendering_program);
            gl::VertexAttrib4fv(0, attributes.as_ptr());
            gl::DrawArrays(gl::TRIANGLES, 0, 3);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
            gl::DeleteProgram(self.0.rendering_program);
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
        }
        Ok(())
    }
}

fn main() -> SBResult {
    Application::new("Moving Triangle").run(MovingTriangle(StateObjects::default()))
}
