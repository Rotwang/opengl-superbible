use gl::types::*;
use glfw::Key;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects, KTX, SBM};
use std::ffi::{c_void, CStr};

#[derive(Default)]
struct SimpleTexCoords {
    s: StateObjects,
    tex_object: [GLuint; 2],
    tex_index: GLuint,
    window_width: i32,
    window_height: i32,
    mv_matrix: GLint,
    proj_matrix: GLint,
    sbm: SBM,
}

impl Renderer for SimpleTexCoords {
    #[allow(non_snake_case)]
    fn startup(&mut self) -> SBResult {
        let B = [0x00 as GLubyte; 4];
        let W = [0xFF as GLubyte; 4];

        #[rustfmt::skip]
        let tex_data = [
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
            B, W, B, W, B, W, B, W, B, W, B, W, B, W, B, W,
            W, B, W, B, W, B, W, B, W, B, W, B, W, B, W, B,
        ];

        unsafe {
            gl::GenTextures(1, &mut self.tex_object[0]);
            gl::BindTexture(gl::TEXTURE_2D, self.tex_object[0]);
            gl::TexStorage2D(gl::TEXTURE_2D, 1, gl::RGB8, 16, 16);
            gl::TexSubImage2D(
                gl::TEXTURE_2D,
                0,
                0,
                0,
                16,
                16,
                gl::RGBA,
                gl::UNSIGNED_BYTE,
                tex_data.as_ptr() as *const c_void,
            );
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::NEAREST as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
        }
        self.tex_object[1] = KTX::load("pattern1.ktx", 0)?;
        self.sbm = Default::default();
        self.sbm.load("torus_nrms_tc.sbm")?;

        let mut make = MakeProgram::new();
        make.compile("05/simpletexcoords.vert")?;
        make.compile("05/simpletexcoords.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            self.mv_matrix = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"mv_matrix\0").as_ptr(),
            );
            self.proj_matrix = gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"proj_matrix\0").as_ptr(),
            );

            gl::Enable(gl::DEPTH_TEST);
            gl::DepthFunc(gl::LEQUAL);
        }

        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let gray: [GLfloat; 4] = [0.2f32, 0.2f32, 0.2f32, 1.0f32];
        let ones: [GLfloat; 1] = [1f32];

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, gray.as_ptr());
            gl::ClearBufferfv(gl::DEPTH, 0, ones.as_ptr());

            //gl::Viewport(0, 0, self.window_width, self.window_height);

            gl::BindTexture(gl::TEXTURE_2D, self.tex_object[self.tex_index as usize]);

            gl::UseProgram(self.s.rendering_program);
        }

        let proj_matrix = vmath::perspective(
            60f32,
            self.window_width as f32 / self.window_height as f32,
            0.1f32,
            1000f32,
        );
        let mv_matrix = vmath::translate(0f32, 0f32, -3f32)
            * vmath::rotate(current_time as f32 * 19.3f32, 0.0f32, 1.0f32, 0.0f32)
            * vmath::rotate(current_time as f32 * 21.1f32, 0.0f32, 0.0f32, 1.0f32);

        unsafe {
            gl::UniformMatrix4fv(self.mv_matrix, 1, gl::FALSE, mv_matrix.as_ptr());
            gl::UniformMatrix4fv(self.proj_matrix, 1, gl::FALSE, proj_matrix.as_ptr());
        }

        self.sbm.render();
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteTextures(2, self.tex_object.as_ptr());
        }
        Ok(())
    }

    fn on_resize(&mut self, w: i32, h: i32) {
        self.window_width = w;
        self.window_height = h;
    }

    fn on_key_pressed(&mut self, key: Key) {
        match key {
            Key::T | Key::Space => {
                self.tex_index += 1;
                if self.tex_index > 1 {
                    self.tex_index = 0;
                }
            }
            _ => {}
        }
    }
}

fn main() -> SBResult {
    let app = Application::new("Texture Coordinates");
    let w: i32 = app.window_width as i32;
    let h: i32 = app.window_height as i32;

    app.run(SimpleTexCoords {
        window_width: w,
        window_height: h,
        ..Default::default()
    })
}
