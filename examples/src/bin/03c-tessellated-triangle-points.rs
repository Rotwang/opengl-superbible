use gl::types::*;

use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};

struct TessellatedTrianglePoints(StateObjects);

impl Renderer for TessellatedTrianglePoints {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile("03/tessellated-triangle-points.vert")?;
        make.compile("03/tessellated-triangle-points.frag")?;
        make.compile("03/tessellated-triangle-points.tesc")?;
        make.compile("03/tessellated-triangle-points.tese")?;
        make.compile("03/tessellated-triangle-points.geom")?;
        self.0.rendering_program = make.link()?;

        unsafe {
            gl::GenVertexArrays(1, &mut self.0.vertex_array_object);
            gl::BindVertexArray(self.0.vertex_array_object);
        }
        Ok(())
    }

    fn render(&mut self, _current_time: f64) {
        let green: [GLfloat; 4] = [0.0f32, 0.25f32, 0.0f32, 1.0f32];

        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());

            gl::UseProgram(self.0.rendering_program);
            gl::PointSize(5.0f32);

            gl::DrawArrays(gl::PATCHES, 0, 3);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
            gl::DeleteProgram(self.0.rendering_program);
        }
        Ok(())
    }
}

fn main() -> SBResult {
    Application::new("Tessellation and Geometry Shaders")
        .run(TessellatedTrianglePoints(StateObjects::default()))
}
