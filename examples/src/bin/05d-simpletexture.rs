use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};
use std::ffi::c_void;

#[derive(Default)]
struct SimpleTexture {
    s: StateObjects,
    texture: GLuint,
}

impl Renderer for SimpleTexture {
    fn startup(&mut self) -> SBResult {
        unsafe {
            // Generate a name for the texture
            gl::GenTextures(1, &mut self.texture);
            // Now bind it to the context using the GL_TEXTURE_2D binding point
            gl::BindTexture(gl::TEXTURE_2D, self.texture);
            // Specify the amount of storage we want to use for the texture
            gl::TexStorage2D(
                gl::TEXTURE_2D, // 2D texture
                8,              // 8 mipmap levels
                gl::RGBA32F,    // 32-bit floating-point RGBA data
                256,
                256,
            ); // 256 x 256 texels
        }
        // Define some data to upload into the texture
        let mut data = [0f32; 256 * 256 * 4];
        // generate_texture() is a function that fills memory with image data
        generate_texture(&mut data, 256, 256);

        unsafe {
            // Assume the texture is already bound to the GL_TEXTURE_2D target
            gl::TexSubImage2D(
                gl::TEXTURE_2D, // 2D texture
                0,              // Level 0
                0,
                0, // Offset 0, 0
                256,
                256,                            // 256 x 256 texels, replace entire image
                gl::RGBA,                       // Four channel data
                gl::FLOAT,                      // Floating point data
                data.as_ptr() as *const c_void, // Pointer to data
            );
        }

        let mut make = MakeProgram::new();
        make.compile("05/simpletexture.vert")?;
        make.compile("05/simpletexture.frag")?;
        self.s.rendering_program = make.link()?;

        unsafe {
            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        Ok(())
    }

    fn render(&mut self, _current_time: f64) {
        let green: [GLfloat; 4] = [0.0f32, 0.25f32, 0.0f32, 1.0f32];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());

            gl::UseProgram(self.s.rendering_program);
            gl::DrawArrays(gl::TRIANGLES, 0, 3);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteVertexArrays(1, &self.s.vertex_array_object);
            gl::DeleteTextures(1, &self.texture);
        }
        Ok(())
    }
}

#[allow(clippy::identity_op)]
fn generate_texture(data: &mut [f32], width: usize, height: usize) {
    for y in 0..height {
        for x in 0..width {
            data[(y * width + x) * 4 + 0] = (((x & y) & 0xFF) as f32) / 255.0f32;
            data[(y * width + x) * 4 + 1] = (((x | y) & 0xFF) as f32) / 255.0f32;
            data[(y * width + x) * 4 + 2] = (((x ^ y) & 0xFF) as f32) / 255.0f32;
            data[(y * width + x) * 4 + 3] = 1.0f32;
        }
    }
}

fn main() -> SBResult {
    let app = Application::new("Simple Texturing");
    app.run(SimpleTexture {
        ..Default::default()
    })
}
