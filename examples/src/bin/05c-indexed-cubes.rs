use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};
use std::ffi::CStr;
use std::mem::size_of_val;
use std::os::raw::c_void;
use std::ptr::null;
use vmath::mat4;

#[derive(Default)]
struct IndexedCubes {
    s: StateObjects,
    window_width: i32,
    window_height: i32,
    index_buffer: GLuint,
    position_buffer: GLuint,
    mv_location: GLint,
    mv_matrix: mat4,
    proj_location: GLint,
    proj_matrix: mat4,
}

impl Renderer for IndexedCubes {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile("05/spinny-cube.vert")?;
        make.compile("05/spinny-cube.frag")?;
        self.s.rendering_program = make.link()?;

        self.mv_location = unsafe {
            gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"mv_matrix\0").as_ptr(),
            )
        };
        self.proj_location = unsafe {
            gl::GetUniformLocation(
                self.s.rendering_program,
                CStr::from_bytes_with_nul_unchecked(b"proj_matrix\0").as_ptr(),
            )
        };

        unsafe {
            gl::GenVertexArrays(1, &mut self.s.vertex_array_object);
            gl::BindVertexArray(self.s.vertex_array_object);
        }

        #[rustfmt::skip]
        let vertex_indices: [GLushort; 36] = [
                0, 1, 2,
                2, 1, 3,
                2, 3, 4,
                4, 3, 5,
                4, 5, 6,
                6, 5, 7,
                6, 7, 0,
                0, 7, 1,
                6, 0, 2,
                2, 4, 6,
                7, 5, 3,
                7, 3, 1
        ];

        #[rustfmt::skip]
        let vertex_positions: [GLfloat; 24] = [
            -0.25f32, -0.25f32, -0.25f32,
            -0.25f32,  0.25f32, -0.25f32,
             0.25f32, -0.25f32, -0.25f32,
             0.25f32,  0.25f32, -0.25f32,
             0.25f32, -0.25f32,  0.25f32,
             0.25f32,  0.25f32,  0.25f32,
            -0.25f32, -0.25f32,  0.25f32,
            -0.25f32,  0.25f32,  0.25f32,
        ];

        unsafe {
            gl::GenBuffers(1, &mut self.position_buffer);
            gl::BindBuffer(gl::ARRAY_BUFFER, self.position_buffer);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                size_of_val(&vertex_positions) as isize,
                vertex_positions.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );
            gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 0, null());
            gl::EnableVertexAttribArray(0);

            gl::GenBuffers(1, &mut self.index_buffer);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, self.index_buffer);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                size_of_val(&vertex_indices) as isize,
                vertex_indices.as_ptr() as *const c_void,
                gl::STATIC_DRAW,
            );

            gl::Enable(gl::CULL_FACE);
            //gl::FrontFace(gl::CW);

            gl::Enable(gl::DEPTH_TEST);
            gl::DepthFunc(gl::LEQUAL);
        }
        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let green: [GLfloat; 4] = [0.0f32, 0.25f32, 0.0f32, 1.0f32];
        let one: GLfloat = 1.0f32;

        unsafe {
            //gl::Viewport(0, 0, self.window_width, self.window_height);
            gl::ClearBufferfv(gl::COLOR, 0, green.as_ptr());
            gl::ClearBufferfv(gl::DEPTH, 0, &one);

            gl::UseProgram(self.s.rendering_program);

            gl::UniformMatrix4fv(self.proj_location, 1, gl::FALSE, self.proj_matrix.as_ptr());

            for i in 0..24 {
                let f = i as f32 + current_time as f32 * 0.3f32;
                self.mv_matrix = vmath::translate(0.0f32, 0.0f32, -6.0f32)
                    * vmath::rotate(current_time as f32 * 45f32, 0f32, 1f32, 0f32)
                    * vmath::rotate(current_time as f32 * 21f32, 1f32, 0f32, 0f32)
                    * vmath::translate(
                        (2.1f32 * f).sin() * 2.0f32,
                        (1.7f32 * f).cos() * 2.0f32,
                        (1.3f32 * f).sin() * (1.5f32 * f).cos() * 2.0f32,
                    );
                gl::UniformMatrix4fv(self.mv_location, 1, gl::FALSE, self.mv_matrix.as_ptr());
                gl::DrawElements(
                    gl::TRIANGLES,
                    36,
                    gl::UNSIGNED_SHORT,
                    std::ptr::null::<c_void>(),
                );
            }
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.s.vertex_array_object);
            gl::DeleteProgram(self.s.rendering_program);
            gl::DeleteBuffers(1, &self.position_buffer);
        }
        Ok(())
    }

    fn on_resize(&mut self, w: i32, h: i32) {
        self.window_width = w;
        self.window_height = h;
        self.proj_matrix = calc_perspective(w, h);
    }
}

fn calc_perspective(w: i32, h: i32) -> mat4 {
    let aspect = w as f32 / h as f32;
    vmath::perspective(50.0f32, aspect, 0.1f32, 1000.0f32)
}

fn main() -> SBResult {
    let app = Application::new("Spinny Cube");
    let w: i32 = app.window_width as i32;
    let h: i32 = app.window_height as i32;
    app.run(IndexedCubes {
        window_width: w,
        window_height: h,
        proj_matrix: calc_perspective(w, h),
        ..Default::default()
    })
}
