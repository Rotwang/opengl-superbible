use gl::types::*;
use sb7::{Application, Renderer, SBResult};

struct ChangingColors;

impl Renderer for ChangingColors {
    fn render(&mut self, current_time: f64) {
        let color: [GLfloat; 4] = [
            (current_time.sin() * 0.5 + 0.5) as GLfloat,
            (current_time.cos() * 0.5 + 0.5) as GLfloat,
            0f32,
            1f32,
        ];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, color.as_ptr());
        }
    }
}

fn main() -> SBResult {
    Application::new("Changing Colors").run(ChangingColors)
}
