use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};

struct SinglePointFlashing(StateObjects);

const VERTEX_SHADER_SOURCE: &str = r#"
    #version 450 core

    void main(void) {
       gl_Position = vec4(0.0, 0.0, 0.5, 1.0);
    }
"#;

const FRAGMENT_SHADER_SOURCE: &str = r#"
    #version 450 core

    out vec4 color;

    void main(void) {
       color = vec4(0.0, 0.8, 1.0, 1.0);
    }
"#;

impl Renderer for SinglePointFlashing {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile_str(VERTEX_SHADER_SOURCE, gl::VERTEX_SHADER)?;
        make.compile_str(FRAGMENT_SHADER_SOURCE, gl::FRAGMENT_SHADER)?;
        self.0.rendering_program = make.link()?;

        unsafe {
            gl::CreateVertexArrays(1, &mut self.0.vertex_array_object);
            gl::BindVertexArray(self.0.vertex_array_object);
        };
        Ok(())
    }

    fn render(&mut self, current_time: f64) {
        let color: [GLfloat; 4] = [
            (current_time.sin() * 0.5 + 0.5) as GLfloat,
            (current_time.cos() * 0.5 + 0.5) as GLfloat,
            0f32,
            1f32,
        ];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, color.as_ptr());
            gl::UseProgram(self.0.rendering_program);
            gl::PointSize(40.0f32);
            gl::DrawArrays(gl::POINTS, 0, 1);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
            gl::DeleteProgram(self.0.rendering_program);
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
        };
        Ok(())
    }
}

fn main() -> SBResult {
    Application::new("Single Point Flashing").run(SinglePointFlashing(StateObjects::default()))
}
