use gl::types::*;
use sb7::{Application, MakeProgram, Renderer, SBResult, StateObjects};

struct SimpleTriangle2(StateObjects);

impl Renderer for SimpleTriangle2 {
    fn startup(&mut self) -> SBResult {
        let mut make = MakeProgram::new();
        make.compile("02/simple-triangle-2.vert")?;
        make.compile("02/simple-triangle-2.frag")?;
        self.0.rendering_program = make.link()?;

        unsafe {
            gl::CreateVertexArrays(1, &mut self.0.vertex_array_object);
            gl::BindVertexArray(self.0.vertex_array_object);
        }
        Ok(())
    }

    fn render(&mut self, _current_time: f64) {
        let color: [GLfloat; 4] = [0.0f32, 0.2f32, 0.0f32, 1.0f32];
        unsafe {
            gl::ClearBufferfv(gl::COLOR, 0, color.as_ptr());
            gl::UseProgram(self.0.rendering_program);
            gl::DrawArrays(gl::TRIANGLES, 0, 3);
        }
    }

    fn shutdown(&mut self) -> SBResult {
        unsafe {
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
            gl::DeleteProgram(self.0.rendering_program);
            gl::DeleteVertexArrays(1, &self.0.vertex_array_object);
        }
        Ok(())
    }
}

fn main() -> SBResult {
    Application::new("Simple Triangle 2").run(SimpleTriangle2(StateObjects::default()))
}
